# About the project
This project includes some notebook files which are shared with students for the course CME4500 Engineering Systems Optimisation.

# Usage
This project included seperate ipynb files which can be viewed in [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.tudelft.nl%2Fcme4500-engineering-systems-optimisation%2Flesson-material/HEAD)
If you encounter any mistakes, please report to Tom van Woudenberg

# Contributors
Tom van Woudenberg - T.R.vanWoudenberg@tudelft.nl
